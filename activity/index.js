/*
	Activity: 
	Create a new http server in index.js using the http module of node.
	This http server should run on port 5000.

	Create routes for the following endpoints:

	1. /
		-write the appropriate headers for our response with writeHead()
			-status 200, Content-Type: text/plain
		-end the response with .end() and send the following message:
		"Welcome to B169 Booking System."

	2. /courses
		-write the appropriate headers for our response with writeHead()
			-status 200, Content-Type: text/plain
		-end the response with .end() and send the following message:
		"Welcome to the Courses Page. View our Courses."

	3. /profile
		-write the appropriate headers for our response with writeHead()
			-status 200, Content-Type: text/plain
		-end the response with .end() and send the following message:
		"Welcome to your Profile. View your details."

	4. add an else which will be a route for the undefined and undesignated endpoints.
		-write the appropriate headers for our response with writeHead()
			-status 404, Content-Type: text/plain
		-end the response with .end() and send the following message:
		"Resource not found."
	
	To run your server, go to your activity folder and open gitbash/terminal then
	run the server with nodemon/ node index.js

*/


let http = require("http");

http.createServer((request, response) => {

    //localhost:4000/ - is called an endpoint
    if (request.url === "/")
    {
        response.writeHead(200, {'Content-Type' : 'text/plain'})
        response.end ('Welcome to B169 Booking System')
    }
    //localhost:4000/login
    else if (request.url === "/courses")
    {
        response.writeHead(200, {'Content-Type' : 'text/plain'})
        response.end('Welcome to the Courses Page. View our Courses')
    }
    else if (request.url === "/profile")
    {
        response.writeHead(200, {'Content-Type' : 'text/plain'})
        response.end('Welcome to your Profile. View your details')
    }
    else
    {
        response.writeHead(404, {'Content-Type' : 'text/plain'})
        response.end('Resource not be found')
    }
    
}).listen(5000)
console.log('Server is running on localhost: 5000');