// Introduction to NodeJS

/*
 
 */

let http = require("http");
    //mopdules needs to be imported or required

console.log(http);
    //http module contains methods and other codes which allowed us to create server and let our client communicate through HTTP



// createServer() - it creates server that handles request and responses

    //.createServer() - method has an anonymouse function that handles our client and our server response. The anonymous function in the createServer method is able to recieve two objects, first, req or request from the client. second, res or response, thisis ourserver response.

    //Each request and response parameters are object which contains the details of a request and response as well the method to handle them.

    //res.writeHead() - a method of the response object. this will allow us to add headers, which are additional information about our server's response. the First argument in writeHead is an HTTP status which is ised to tell the client about the status  of their request

    //res.end() - is a method of a response object which end the server's response and sends a message as a string

    //.list() -assign a port to a server. There are several tasks and process in our computer which are also designated into their specific ports

    //http://localhost:4000
        //localhost - current machine
        //4000 - port number assigned to where the process/ serveris listening/running from




http.createServer((req, res) => {

    //localhost:4000/ - is called an endpoint
    if (req.url === "/")
    {
        res.writeHead(200, {'Content-Type' : 'text/plain'})
        res.end ('Hi Welcome to our Homepage')
    }
    //localhost:4000/login
    else if (req.url === "/login")
    {
        res.writeHead(200, {'Content-Type' : 'text/plain'})
        res.end('Welcome to the login page!')
    }
    else
    {
        res.writeHead(404, {'Content-Type' : 'text/plain'})
        res.end('Resource cannot be found')
    }

}).listen(4000)
console.log('Server is running on localhost: 4000');
